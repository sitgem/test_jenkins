import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormGroup,
  FormFeedback,
  CustomInput,
  Label,
  Row,
  Table,
  UncontrolledTooltip
} from 'reactstrap';
import PanelHeader from "../../template/panelHeader/PanelHeader";

import { post, notification }  from "../../services";

export default class CreditCardUpdate extends React.Component {
  _isMounted = false;
  constructor(props){
    super(props);
    this.state = {
      file:null,
      filename:null,
      file_valid:null,
      payment_gateway:null,
      payment_gateway_valid:null,
      button_disable:true,
      response_list:false

    };
    this.validTypesFile = [
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "application/vnd.ms-excel",
      "application/vnd.oasis.opendocument.spreadsheet"
    ]
    this.listResponse = []
  }

  async onSubmit(e){
    e.preventDefault() 
    const formData = new FormData();
    formData.append('file',this.state.file)
    formData.append('payment_gateway',this.state.payment_gateway)
    let result = await post('/sas/credit-card-update', formData)
    if(result.success){
      this.listResponse = result.data.data
      this.setState({
        file:null,
        filename:null,
        file_valid:null,
        payment_gateway:null,
        payment_gateway_valid:null,
        button_disable:true,
        response_list:true
      })
      notification('Procesado')
    }
  }

  onChange(e) {
    let state = this.state
    state.button_disable = true
    if(e.target.name==='payment_gateway'){
      state.payment_gateway = e.target.value
      state.payment_gateway_valid = (e.target.value!=='')? true : false
    }
    else if(e.target.name==='file'){
      state.file_valid = false
      let file = e.target.files
      if(file.length>0){
        file = file[0]
        state.file = file
        state.filename = (file.name)? file.name : '' 
      }

      if(this.validTypesFile.includes(file.type)){
        state.file_valid = true
      }
    }
    if(state.payment_gateway_valid && state.file_valid){
        state.button_disable = false
    }
    this.setState(state)
  }
  
  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {

    if(!this._isMounted) return null

    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <h5 className="title">Credit Card Update</h5>
                </CardHeader>
                <CardBody>
                 <Form>
                  <Row>
                    <Col className="pc-1" md="3">
                      <FormGroup>
                        <label >Payment Gateway </label>
                        <CustomInput 
                          type="select" 
                          name="payment_gateway" 
                          id="payment_gateway" 
                          bsSize="sm" 
                          style={{height: 32}}
                          defaultValue = {this.state.payment_gateway}
                          onChange={ this.onChange.bind(this) }
                          valid={(this.state.payment_gateway_valid)}
                          invalid={(!this.state.payment_gateway_valid && this.state.payment_gateway_valid!=null)}  
                          >
                            <option value="">Please select</option>
                            <option value="naranja">Naranja</option>
                            <option value="decidir">Decidir</option>
                          </CustomInput>
                         <FormFeedback>Debe seleccionar uno</FormFeedback>
                      </FormGroup>
                    </Col>
                    <Col className="pc-1" md="6">
                      <FormGroup>
                        <label>Archivo (xls,xlsx)</label>
                        <CustomInput 
                          type="file" 
                          name="file" 
                          id="file" 
                          style={{height: 32}}
                          onChange={ this.onChange.bind(this) }
                          valid={(this.state.file_valid)}
                          invalid={(!this.state.file_valid && this.state.file_valid!=null)}
                          label={(this.state.filename==null)?"Subir archivo": this.state.filename} />
                          <FormFeedback>solo archivos (xls,xlsx)</FormFeedback>
                      </FormGroup>
                    </Col>
                    <Col  className="pc-1" md="3">
                      <FormGroup>
                        <Label></Label>
                        <div>
                          <Button style={{height: 32,marginLeft: 0,marginTop: 4}}
                            type="submit" 
                            size="sm" 
                            color="primary" 
                            outline={this.state.button_disable}
                            disabled={this.state.button_disable}
                            onClick={this.onSubmit.bind(this)}
                          >
                            <i className="fa fa-user"></i> Procesar
                          </Button>
                        </div>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row style={(!this.state.response_list)? { display: 'none' } : {} }>
                    <Col xs={12}>
                      <Table responsive>
                        <thead className="text-primary">
                          <tr>
                            <th className="text-right">UserId</th>
                            <th className="text-right">Old Tarjeta</th>
                            <th className="text-right">New Tarjeta</th>
                            <th className="text-right">Procesado</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.listResponse.map((prop, key) => {
                            return (
                              <tr key={key}>
                                <td className="text-right">{prop.id}</td>
                                <td className="text-right">{prop.old_tarjeta}</td>
                                <td className="text-right">{prop.new_tarjeta}</td>
                                <td className="text-center" id={"tooltip"+key}>    
                                  <i className={(prop.matched)?"fa fa-check text-success":"fa fa-times text-danger"} />
                                </td>
                                <UncontrolledTooltip delay={0} target={"tooltip"+key}>
                                  {(prop.matched)? 'Procesado' : 'Sin Procesar' }
                                </UncontrolledTooltip>
                              </tr>
                            )
                          })}
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              </Card>
            </Col>
          </Row>
          
        </div>
      </div>
    );
  }
}