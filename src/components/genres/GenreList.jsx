import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  Row,
  Table,
  UncontrolledTooltip
} from 'reactstrap';
import PanelHeader from "../../template/panelHeader/PanelHeader";

import { get }  from "../../services";

class GenreList extends React.Component {
  _isMounted = false;
  constructor(props){
    super(props);
    this.state = {
      response_list:true

    };

    this.listResponse = []
  }

  async componentWillMount() {
    this._isMounted = true;
    let result = await get('/genres')
    if(result.success){
      this.listResponse = result.data.data.elements
      this.setState({
        response_list:true
      })
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {

    if(!this._isMounted) return null

    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <h5 className="title">Generos</h5>
                </CardHeader>
                <CardBody>
                 <Form>
                  <Row style={(!this.state.response_list)? { display: 'none' } : {} }>
                    <Col xs={12}>
                      <Table responsive>
                        <thead className="text-primary">
                          <tr>
                            <th className="text-lefth">Nombre</th>
                            <th className="text-center">Visible</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.listResponse.map((prop, key) => {
                            let img = (prop.images[0]===undefined)? {} : prop.images[0]
                            return (
                              <tr key={key}>
                                <td className="text-lefth">
                                <img alt={prop.images[0].name}
                                  style={{ marginLeft:10, borderRadius: 15, width: 30, height: 30}} src={img.source} /> 

                                 <span style={{marginLeft:30}}>{'  '+ prop.name}</span>
                                </td>
                                <td className="text-center" id={"tooltip"+key}>    
                                  <i className={(prop.visible)?"fa fa-check text-success":"fa fa-times text-danger"} />
                                </td>
                                <UncontrolledTooltip delay={0} target={"tooltip"+key}>
                                  {(prop.visible)? 'Visible' : 'Oculto' }
                                </UncontrolledTooltip>
                              </tr>
                            )
                          })}
                        </tbody>
                      </Table>
                    </Col>
                  </Row>
                </Form>
              </CardBody>
              </Card>
            </Col>
          </Row>
          
        </div>
      </div>
    );
  }
}

export default GenreList;
