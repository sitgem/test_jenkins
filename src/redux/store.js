import { createStore, applyMiddleware, combineReducers } from 'redux';

import thunk from 'redux-thunk';

const status = (state=null, action) =>{
	return action.type;
}

const auth = (state=null, action) =>{

	if(action.type==="AUHTS"){
		return action.auth; 
	}
	return state;
}

const environment = (state=null, action) =>{

	if(action.type==="ENVIRONMENTS"){
		return action.environment; 
	}
	return state;
}

const password = (state=null, action) =>{

	if(action.type==="PASSWORDS"){
		return action.password; 
	}
	return state;
}

const profile = (state=null, action) =>{

	if(action.type==="PROFILES"){
		return action.profile; 
	}
	return state;
}

const notification = (state=null, action) =>{

	if(action.type==="NOTIFICATIONS"){
		return action.notification; 
	}
	return state;
}

const logger = store => next => action => {

	/*
		primer_state = store.getState().entities
		siguiente_state = action
	*/
	let result = next(action);
	//console.log('action', action)
	//console.log('result', result)
  	return result;
}

const rootReducer = combineReducers({
	status,
	auth,
	profile,
	notification,
	environment,
	password
})


//el primero recibe la funcion reduce y el segundo es el estado inicial que es un objeto
export default createStore(rootReducer, applyMiddleware(logger, thunk));