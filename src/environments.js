const apiKey = '9088-697E'
const apiVersion = '6.0'
const apiSecret = '5fd2a7181825445eabd5edfdee1345baab942b2902c42'

export default {
    name:'QubitTV',
    default:'local',
    environments:[
        {
            name:'local',
            apiBaseUrl: 'http://juanmanuel.localdev.qubit.tv:9001',
            apiKey,
            apiSecret,
            apiVersion,
        },
        {
            name:'stage',
            apiBaseUrl: 'http://api.stage.qubit.tv',
            apiKey,
            apiSecret,
            apiVersion,
        },
        {
            name:'preprod',
            apiBaseUrl: 'http://api.preprod.qubit.tv',
            apiKey,
            apiSecret,
            apiVersion,
        },
        {
            name:'production',
            apiBaseUrl: 'http://api.qubit.tv',
            apiKey,
            apiSecret,
            apiVersion,
        }
    ]
}

