import MassCancel from './components/sas/MassCancel';
import CreditCardUpdate from './components/sas/CreditCardUpdate';
import GenreList from './components/genres/GenreList';
import NotFound from './template/notFound/NotFound';

export default [
	{menu:false, name:'not-found', path:'/not-found', component: NotFound, },
	
	{menu:true,  name:'CreditCardUpdate', path:'/credit-card-update', component: CreditCardUpdate, 	icon:'now-ui-icons business_briefcase-24'},
	{menu:true,  name:'MassCancel', path:'/mass-cancel', component: MassCancel, icon:'now-ui-icons shopping_box'},
	{menu:true,  name:'Generos', path:'/genres', component: GenreList, icon:'now-ui-icons transportation_air-baloon'},
]
