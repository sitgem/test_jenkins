import React from "react";
import { render } from 'react-dom';
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect, BrowserRouter } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.css";
import "./assets/scss/template.scss?v1.2.0";

import { Provider } from 'react-redux';
import store from './redux/store';

import { auth } from './services';

import Qubit from './layout/Qubit';
import Login from './template/login/Login';
import Signup from './template/signup/Signup';

const hist = createBrowserHistory();
auth()
render(
	<Provider store={store}>
		<Router history={hist}>
		<BrowserRouter>
		    <Switch>
		    	<Route path="/qubit" render={ props => <Qubit {...props} />} />
			    <Route history={hist} path="/login" component={() => <Login history={hist} />} exact />
		        <Route path="/signup" component={Signup} exact/>
		        <Redirect from="/" exact to="/qubit/mass-cancel" />
		    	<Route path="*" render={ props => <Qubit {...props}/>} /> 
		    </Switch>
		</BrowserRouter>
		</Router>
  	</Provider>,
    document.getElementById('root')
)
