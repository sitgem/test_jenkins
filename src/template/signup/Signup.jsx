import React, {Component} from 'react';
//import { Redirect } from "react-router-dom";

//import {Post} from '../../services/Http';
//import {tokenValidate, tokenReadStore} from '../../services/Token';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';



class Signup extends Component {

  constructor(props){
    super(props);
   
    this.state = {
     email: '',
     password: '',
     password_confirmation: '',
     session:false,
     timeSession:null
    };

    this.signup = this.signup.bind(this);
    this.onChange = this.onChange.bind(this);

  }

  componentDidMount() {
    //this.setState({ timeSession: tokenReadStore('timeSession')});
    
  }
 
  signup() {
    /*
    if(this.state.email && this.state.password && this.state.password_confirmation){

      Post('signup',this.state)
        .then((responseJson) => {

          console.log(responseJson.register);
          if(responseJson.register){  
            this.setState({session: true});
          }
          else{
            console.log(responseJson.message);
          }
      });
    }
    */
  }

 onChange(e){
   this.setState({[e.target.name]:e.target.value});
  }

  render() {
    /*
    if (!tokenValidate(this.state.timeSession)) {
      
      return (<Redirect to={'/express'}/>)
    }
    */

    return (
      <div>
        <span><br /></span>
        <span><br /></span>
        <span><br /></span>
        <span><br /></span>
        <Container>
          <Row>
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Register</h1>
                      <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                        <Input type="text" name="email" placeholder="Email" autoComplete="email" onChange={this.onChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" name="password" placeholder="Password" autoComplete="current-password" onChange={this.onChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" name="password_confirmation" placeholder="Repeat Password" autoComplete="current-password" onChange={this.onChange} />
                      </InputGroup>

                      <Row>
                          <Button color="primary" onClick={this.signup}>Create Account</Button>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '100%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Login</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <a href="/login"><Button color="primary" className="mt-3" active tabIndex={-1}>Login</Button></a>
                    </div>
                      <a href="/login"><Button color="link" className="mt-3" tabIndex={-1}>Forgot password?</Button></a>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
       </div>
    );
  }
}

export default Signup;