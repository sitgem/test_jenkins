import React from 'react';

import NotificationAlert from "react-notification-alert";


class Notifications extends React.Component {
	
	constructor(props){

	    super(props);
	    this.notificationsCenter = this.notificationsCenter.bind(this);
	}

 	notificationsCenter() {

 		let notification = this.props.reduxStore.notification
	    if(notification!==null){

	      let options = {
	        place: 'tr',
	        icon: "now-ui-icons ui-1_bell-53"
	      };
	      options.autoDismiss = notification.time;
	      options.type = notification.type;

	      if(notification.data.success===true){
	      	options.message= (<div>{notification.data.message}</div>) 
	      }
	      else if(notification.data.success===false){
	      	options.message= (<div><b>{notification.data.code}</b> {notification.data.message}</div>)
	      }
	      else{
	      	options.message= (<div>{notification.data}</div>) 
	      }
	      this.refs.notify.notificationAlert(options);
	      this.props.reduxActions.notifications(null)

	    }
  	}
  	componentDidMount() {
		this.notificationsCenter()
  	}
	componentDidUpdate() {
		this.notificationsCenter()
	}
  	render() {
  		return <NotificationAlert ref="notify" />
  	}
}
export default Notifications;
