import React from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
} from 'reactstrap';
import PanelHeader from "../../template/panelHeader/PanelHeader";

export default class NotFound extends React.Component {
  _isMounted = false;
  constructor(props){
    super(props);
    this.state = {
    };

  }
  
  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {

    if(!this._isMounted) return null
  	
    return (
      <div>
        <PanelHeader size="sm"/>
        <div className="content">
          <Row>
            <Col>
              <Card style={{background:'#ececec'}}>
                <CardHeader>
                <div style={{ flex:1, flexDirection: 'row',justifyContent: "space-between", alignItems: 'center'}}>
                  <i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
                  <span className="title" style={{  fontSize:40}}>Error 404:</span>
                  <span style={{  fontSize:12,color:'#ccccc'}}> &nbsp;&nbsp;&nbsp;&nbsp;{this.props.location.errorPath}</span>
                  </div>
                </CardHeader>
                <CardBody>
                    <img s={12} alt={'Error 404'} 
			            style={{ width: '100%'}}
			            src={require('../../assets/img/SVG-Animation-404-Page.gif')} 
			        /> 
                      
              </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
