import React, {Component} from 'react';
import { 
  //Link, 
  Redirect 
} from 'react-router-dom';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row,
DropdownToggle, DropdownMenu, DropdownItem, InputGroupButtonDropdown
 } from 'reactstrap';

import Notifications from "../notifications/Notifications";

import { connect } from 'react-redux';
import { notifications } from '../../redux/actions'
import store  from '../../redux/store'

import { request, storeProfile, auth ,setEnvironments } from '../../services';

import env  from "../../environments"

class Login extends Component {
  _isMounted= false;
  constructor(props){

    super(props);
    this.state = {
      email_value:null,
      email_valid:null,
      email_min:5,
      pass_value:null,
      pass_valid:null,
      pass_min:4,
      button_disabled:true,
      isOpenDropdown:false,
      environment:this.props.reduxStore.environment,
      changeEnv:false
    }
  }

  onChange(e){

    let states = this.state;
    let value = e.target.value
    let length = value.length

    if(e.target.name==='email'){
      states.email_valid = false
      if((value.indexOf("@") > -1) && (value.indexOf(".") > -1)){
        states.email_valid = (length>=states.email_min)? true : false
      }
      states.email_value = (value==='')? null : value;
    }
    else if(e.target.name==='password'){
      states.pass_valid = false
      states.pass_valid = (length>=states.pass_min)? true : false
      states.pass_value = (value==='')? null : value;
    }
     states.button_disabled = true
    if(states.email_valid && states.pass_valid){
      states.button_disabled = false
    }
    this.setState(states);
  }

  async onSubmit(e){
    this.setState({button_disabled:true});
    e.preventDefault()
    let profile = await request('/login/password',{
      data:{
        'Username':this.state.email_value,
        'Password':this.state.pass_value
      },
      method: 'post'
    })
    if(profile.success){
      profile =profile.data.data
      profile.password = this.state.pass_value
      storeProfile(profile)
    }
  }
  
  componentDidMount(){
    store.subscribe( () => {
      if(store.getState().status==='PROFILES'){
        //if(this._isMounted) this.setState({update:!this.state.update})
      }
    })
  }
  capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {

    if(!this._isMounted) return null

    if(this.props.reduxStore.profile!==null){
      return (<Redirect to={{ pathname: '/qubit/mass-cancel'}} /> )
    }
   
    return (

      <div>
        {
         // JSON.stringify(this.props.reduxStore.auth)
        }
        <span><br /></span>
        <div className="d-none d-sm-none d-md-block">
          <span><br /></span>
          <span><br /></span>
          <span><br /></span>
          <span><br /></span>
          <span><br /></span>
        </div>
        <Notifications {...this.props} />
        <span><br /></span><span><br /></span>
          <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                     <Row>
                        <Col xs="12">
                          <h1>Login</h1>
                          <p className="text-muted">Iniciar sesión</p>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-user"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input type="text" name="email" placeholder="Email" autoComplete="email" 
                              onBlur={this.onChange.bind(this)}
                              valid={(this.state.email_valid) }
                              invalid={(!this.state.email_valid)  && this.state.email_value!==null} 
                            />
                          </InputGroup>
                          <InputGroup className="mb-4">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-lock"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input type="password" name="password" placeholder="Password" autoComplete="current-password" 
                              onChange={this.onChange.bind(this)} 
                              onBlur={this.onChange.bind(this)}
                              valid={(this.state.pass_valid) }
                              //invalid={(!this.state.pass_valid)  && this.state.pass_value!==null} 
                            />
                          </InputGroup>
                        </Col>
                      </Row>
                      <Row>
                        <Col xs="12">
                        <div className="text-center">
                              <InputGroup>
                              <Button 
                                //color="primary"
                                className={"px-4 "}
                                onClick={this.onSubmit.bind(this)}
                                disabled={this.state.button_disabled}
                                style={{background:'#020a38'}}
                                >
                                <i className="fa fa-user"></i>
                                Login
                              </Button>
                                <InputGroupButtonDropdown addonType="append"
                                  isOpen={this.state.isOpenDropdown}
                                  toggle={() => { this.setState({ isOpenDropdown: !this.state.isOpenDropdown }); }}>
                                <DropdownToggle caret color="primary"
                                  style={{background:'#020a38', display: (this.props.reduxStore.auth!==null && this.state.changeEnv===false)? ' none': ''}}
                                //disabled={this.state.button_disabled}
                                >
                                  {this.capitalize(this.state.environment)}
                                </DropdownToggle>
                                <DropdownMenu className={this.state.isOpenDropdown ? 'show' : ''}>
                                  {env.environments.map((item, key) => {
                                    if(this.state.environment!==item.name){
                                      return <DropdownItem 
                                      onClick={() => {
                                        setEnvironments(item.name)
                                        auth()
                                        let button_disabled = true
                                        if(this.state.email_value!==null && this.state.pass_value!==null){
                                          button_disabled = false
                                        }
                                        this.setState({ button_disabled, changeEnv:true, environment: item.name }) 
                                      }}
                                      key={key}>{this.capitalize(item.name)}</DropdownItem>
                                    }
                                    return null
                                  })}
                                </DropdownMenu>
                              </InputGroupButtonDropdown>
                            </InputGroup>
                            </div>
                        </Col>
                      </Row>
                        {/*
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                        */}
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white py-5 d-none d-sm-none d-md-block" style={{background:'#020a38'}} xs="12">
                  <CardBody className="text-center">

                  <img alt={'QubitTV'} style={{ width: '60%', marginTop: 20,}}
                   src={require('../../assets/img/favicon-qubit.png')} 
                   /> 

                {/*
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Link to="/signup">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
              */}
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => {

  return {
    reduxStore:{
      profile:state.profile,
      notification:state.notification,
      auth:state.auth,
      environment:state.environment,
    }
  }
}
const mapDispatchToProps = dispatch => {

  return {
    reduxActions:{
      notifications(value){ dispatch(notifications(value)) },
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
