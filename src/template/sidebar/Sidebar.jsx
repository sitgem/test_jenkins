/*eslint-disable*/
import React from "react";
import { NavLink } from "react-router-dom";
import { Nav } from "reactstrap";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";

import './Sidebar.css';

var ps;

export default  class Sidebar extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.activeRoute.bind(this);
  }
  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return (this.props.location.pathname===routeName) ? "active" : "";
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.sidebar, {
        suppressScrollX: true,
        suppressScrollY: false
      });
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }
  componentWillMount() {
    this._isMounted = true;
  }

  render() {

    if(!this._isMounted) return null
    return (
      <div className="sidebar" data-color={this.props.backgroundColor}>
        <div className="logo" style={{marginTop: 0,  height: 100, }}>
          <img alt={'QubitTV'} 
            className="simple-text logo-normal"
            style={{ width: '80%'}}
            src={require('../../logo-qubit-azul.svg')} 
          /> 
            <div className="text-right"
             style={{  fontSize:12,textTransform: 'capitalize', width: '80%', color:'#fff'}}
            >
              {this.props.environment}
          </div>
        </div>
        <div className="sidebar-wrapper" ref="sidebar">
          <Nav>
            {this.props.routes.map((prop, key) => {
              if(!prop.menu) return null
              return (
                <li
                  className={this.activeRoute('/'+this.props.layout + prop.path)}
                  key={key}
                >
                  <NavLink
                    to={'/'+this.props.layout + prop.path}
                    className="nav-link"
                    activeClassName="active"
                  >
                    <i className={(prop.icon===undefined)? 'now-ui-icons travel_info' : prop.icon} />
                    <p>{prop.name}</p>
                  </NavLink>
                </li>
              );
            })}
          </Nav>
        </div>
      </div>
    );
  }
}
