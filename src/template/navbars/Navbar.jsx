import React from "react";
import { Link } from "react-router-dom";
import { Table,
  Collapse, 
  Navbar, 
  NavbarToggler, 
  Nav, 
  NavItem, 
  Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Container,
  Button, Modal, ModalBody, ModalFooter, ModalHeader,
  InputGroup, Input, InputGroupAddon, InputGroupText,
  FormGroup,
  Label,
  Form,
  Col,
  //NavbarBrand,

} from "reactstrap";


import { logout, changeEnv } from '../../services';
import env  from "../../environments"

class Header extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      username:null,
      password:null,
      isOpen:false,
      dropdownAmbienteOpen: false,
      dropdownProfileOpen: false,
      color: "transparent",
      modalLogout: false, 
      modalEnv: false, 
      modalPass: false, 
      classes: "dropdown show",
      button_disable:true
    };
    this.presentEnvironment=null
    this.tmpEnvironment=null
    this.toggleModalLogout = this.toggleModalLogout.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.setState({ classes: (this.state.classes === "dropdown") ? "dropdown show" : "dropdown" });
  }

  logout(){
    //alert('salir');
    logout()

    //tokenRemoveStore();
    //this.props.reduxActions.tokenLoadStore();
  }

  sidebarToggle = React.createRef();

  toggle = () => {

    this.setState({ color: (this.state.isOpen) ? "transparent" : "white" });
    this.setState({ isOpen: !this.state.isOpen });
  };

 
  openSidebar = () => {
    document.documentElement.classList.toggle("nav-open");
    this.sidebarToggle.current.classList.toggle("toggled");
  };

  // function that adds color white/transparent to the navbar on resize (this is for the collapse)
  updateColor = () => {
    this.setState({ color: (window.innerWidth < 993 && this.state.isOpen) ? "white" : "transparent" });
  };


  componentDidMount() {
    this.presentEnvironment = this.props.environment
    this.setState({ username:this.props.profile.username});
    window.addEventListener("resize", this.updateColor.bind(this));
  }

  selectEnv(environment){
    if(this.presentEnvironment !== environment){
      this.setState({ button_disable: false, isOpen:false, modalPass:(this.props.reduxStore.password===null)? true:false });
      this.modalEnv()
      if(this.props.reduxStore.password!==null){
        this.authEnv(this.props.reduxStore.password, environment)
      }
      else{
        this.tmpEnvironment = environment
        this.modalPass()
      }
    }
  }
  authEnv(pass, environment){

    this.presentEnvironment=environment
    this.setState({ button_disable: false });
    changeEnv({
      environment:environment,
      username:this.state.username,
      password:pass
    })
  }

  componentDidUpdate(e) {
    if (
      window.innerWidth < 993 &&
      e.history.location.pathname !== e.location.pathname &&
      document.documentElement.className.indexOf("nav-open") !== -1
    ) {
      document.documentElement.classList.toggle("nav-open");
      this.sidebarToggle.current.classList.toggle("toggled");
    }
  }

  toggleModalLogout() {
    this.setState({ modalLogout: !this.state.modalLogout, })
  }

  modalEnv() {
    this.presentEnvironment=this.props.environment
    this.setState({ modalEnv: !this.state.modalEnv})
  }
    
  modalPass(){
    this.setState({ modalPass: !this.state.modalPass})
  }

  capitalize(str){
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  onChange(e) {
    let state = this.state
    state.button_disable = true
    if(e.target.name==='user'){
      state.username = this.state.username
    }
    else if(e.target.name==='password'){
      state.password = e.target.value
      state.button_disable = false
    }
    this.setState(state)
  }

  componentWillMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {

    if(!this._isMounted) return null
    if(this.props.reduxStore.profile===null) return null

    let profile = this.props.reduxStore.profile.status
    return (
      // add or remove classes depending if we are on full-screen-maps page or not
      <Navbar
        color={this.props.location.pathname.indexOf("full-screen-maps") !== -1 ? "white" : this.state.color }
        expand="lg"
        className={
          this.props.location.pathname.indexOf("full-screen-maps") !== -1
            ? "navbar-absolute fixed-top"
            : "navbar-absolute fixed-top " +
              (this.state.color === "transparent" ? "navbar-transparent " : "")
        }
      >
      <Modal isOpen={this.state.modalLogout} toggle={this.toggleModalLogout} className={'modal-primary ' + this.props.className}>
        <ModalHeader toggle={this.toggleModalLogout}>Cerrar Sesión</ModalHeader>
        <ModalBody>
          Cerrar Sesion de usuario: <b>{profile.username}</b>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" style={{background:'#020a38'}} onClick={this.logout.bind(this)}>Aceptar</Button>{' '}
          <Button color="secondary" onClick={this.toggleModalLogout}>Cancel</Button>
        </ModalFooter>
      </Modal>

      <Modal isOpen={this.state.modalEnv} toggle={this.modalEnv.bind(this)} className={'modal-primary ' + this.props.className}>
        <ModalHeader toggle={this.modalEnv.bind(this)}>Cambio de Environments</ModalHeader>
        <ModalBody>
          
          <Table responsive>
            <tbody>
              {env.environments.map((item, key) => {
                  let name = this.capitalize(item.name)
                  return (
                  <tr key={key}>
                    <td className="text-lefth"></td>
                    <td className="text-lefth">{(this.presentEnvironment===item.name)? <span><b>{name}</b> (actual)</span> : name}</td>
                    <td className="text-right">
                      <FormGroup check>
                        <Label check>
                          <Input defaultChecked={(this.presentEnvironment===item.name)} disabled={(!this.presentEnvironment===item.name)} type="checkbox" onClick={() => this.selectEnv(item.name)}/>
                          <span className="form-check-sign" />
                        </Label>
                      </FormGroup>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </ModalBody>
          {/*
        <ModalFooter>
          <Button color="primary" disabled={this.state.button_disable} onClick={this.logout.bind(this)}>Aceptar</Button>{' '}
          <Button color="secondary" onClick={this.modalEnv.bind(this)}>Cancel</Button>
        </ModalFooter>
          */}
      </Modal>

       <Modal isOpen={this.state.modalPass} toggle={this.modalPass.bind(this)} className={'modal-primary ' + this.props.className}>
        <ModalHeader toggle={this.modalPass.bind(this)} className={'center'}>Indica tu Password</ModalHeader>
        <ModalBody>
            
            <Form>
                <FormGroup row>
                    <Col md="12">
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-user"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="text"  name="user" 
                    defaultValue = {this.state.username} 
                    onChange={this.onChange.bind(this)} 
                    onBlur={this.onChange.bind(this)}
                  />
                </InputGroup>
              </Col>
            </FormGroup>
             <FormGroup row>
                    <Col md="12">
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="fa fa-asterisk"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input type="password" name="password" 
                  placeholder="Password" autoComplete="current-password"
                  onChange={this.onChange.bind(this)} 
                  onBlur={this.onChange.bind(this)}
                  />
                </InputGroup>
              </Col>
            </FormGroup>
          </Form>


        </ModalBody>
        <ModalFooter>
          <Button color="primary" style={{background:'#020a38'}} disabled={this.state.button_disable} 
            onClick={() => {
              this.authEnv(this.state.password, this.tmpEnvironment)
              this.modalPass()
            }}>
            Aceptar
          </Button>
          {' '}
          <Button color="secondary" onClick={this.modalPass.bind(this)}>Cancel</Button>
        </ModalFooter>
      </Modal>



        <Container fluid>
          <div className="navbar-wrapper">
            <div className="navbar-toggle">
              <button
                type="button"
                ref={this.sidebarToggle}
                className="navbar-toggler"
                onClick={() => this.openSidebar()}
              >
                <span className="navbar-toggler-bar bar1" />
                <span className="navbar-toggler-bar bar2" />
                <span className="navbar-toggler-bar bar3" />
              </button>
            </div>
            {/*
              <NavbarBrand href="/">{this.presentEnvironment}</NavbarBrand>
            */}
          </div>
          <NavbarToggler onClick={this.toggle}>
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
            <span className="navbar-toggler-bar navbar-kebab" />
          </NavbarToggler>
          <Collapse
            isOpen={this.state.isOpen}
            navbar
            className="justify-content-end"
          > 
            {/*
            <form>
              <InputGroup className="no-border">
                <Input placeholder="Search... " />
                <InputGroupAddon addonType="append">
                  <InputGroupText>
                    <i className="now-ui-icons ui-1_zoom-bold" />
                  </InputGroupText>
                </InputGroupAddon>
              </InputGroup>
            </form>
          */}
            <Nav navbar>
              <NavItem onClick={() => this.modalEnv()}>
                <Link to="#" className="nav-link">
                  <i className="now-ui-icons media-2_sound-wave" />
                  <p>
                    <span className="d-lg-none d-md-block">Ambientes</span>
                  </p>
                </Link>
              </NavItem>
              <Dropdown nav isOpen={this.state.dropdownProfileOpen} 
              toggle={(e) => {
                this.setState({ dropdownProfileOpen: !this.state.dropdownProfileOpen});
              }} 
              >
                <DropdownToggle caret nav>
                  <i className="now-ui-icons users_single-02" />
                  <p>
                    <span className="d-lg-none d-md-block">Profile</span>
                  </p>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem tag="a">{profile.username}</DropdownItem>
                  <DropdownItem tag="b" className="d-none d-sm-none d-md-block" onClick={this.toggleModalLogout}>Salir</DropdownItem>
                </DropdownMenu>
              </Dropdown>
              <NavItem className="d-lg-none d-md-block">
                <Link to="#pablo" className="nav-link">
                  <i className="now-ui-icons media-2_sound-wave" />
                  <p>
                    <span className="d-lg-none d-md-block" onClick={this.toggleModalLogout}>Salir</span>
                  </p>
                </Link>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}
export default Header;
