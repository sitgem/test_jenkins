import axios from 'axios';
import env from './environments';
import store from './redux/store';

export function get(url, data = {}){
  /*
  if(null===store.getState().profile.status){
    notification('Sesión Cerrada', 'warning', 3)
    return {success:false}
  }
  else{
  }
  */
  return request(url, {method: 'get'})
}

export function post(url, data = {}){
  /*
  if(null===store.getState().profile.status){
    notification('Sesión Cerrada', 'warning', 3)
    return {success:false}
  }
  else{
    
  }
  */
  return request(url, {data:data, method: 'post'})
}


//AUTH
export function validateAuth(envDefault=null){
  let state = store.getState().auth
  let valid = false

  state = (state===null)? JSON.parse(localStorage.getItem('@qubit-auth') ) : state

  if(state!==null && state.fin > (new Date()).getTime()){
    if(envDefault==null){
      valid = true
    }
  }
  return {valid:valid, state:state}
}
export function storeAuth(auth=null){
  store.dispatch(
    dispacth => {
        dispacth({
        type:"AUHTS",
        auth
      })
    } 
  )
}

export async function auth(envDefault=null){
  let data = {}
  let validate = validateAuth(envDefault)
  if(envDefault===null){
    setEnvironments(envDefault)
  }
  let environment = getEnvironment(envDefault)
  if(!validate.valid){
    
      data.api_key=environment.apiKey
      data.api_secret=environment.apiSecret
      data.api_version=environment.apiVersion
      let obj = (envDefault===null)? {data:data, method: 'post'} : {data:data, method: 'post',environment:environment.name}
      let auth = await request('/auth', obj)
      if(auth.success){
        auth = auth.data.data
        let date = new Date()
        auth.ini = date.getTime()
        date.setMinutes(date.getMinutes() + 50)
        auth.fin = date.getTime()
        localStorage.setItem('@qubit-auth',JSON.stringify(auth))
        storeAuth(auth)
        if(envDefault==null){
          storeProfile({status:null,profiles:null, password:null})
        }
        return true
      }
      else{
        return false
      }
  }
  else{
    let status = JSON.parse(localStorage.getItem('@qubit-profile'))
    let profiles = null
    if(envDefault===null){
      await storeProfile({
        status:status,
        profiles:profiles,
        password:null
      })
      let ping = await get('/ping')
      if(ping.success===false){
        validate.state = null
        storeAuth(null)
        return false
      }
    }
    storeAuth(validate.state)
    return true
  }
}

export async function changeEnv(obj){
  let _auth = await auth(obj.environment)
  if(_auth){
    let profile = await request('/login/password',{
      data:{
        'Username':obj.username,
        'Password':obj.password
      },
      method: 'post',
      environment:obj.environment
    })
   
    if(profile.success){
      profile =profile.data.data
      profile.password = obj.password
      storeProfile(profile)
      setEnvironments(obj.environment)
    }
  }

}

export function setEnvironments(envDefault=null){

  let environment = store.getState().environment
  if(envDefault===null){
    environment = localStorage.getItem('@qubit-environment')
    if(environment===null){
      envDefault = env.default
      environment = env.environments.find(environment => environment.name === envDefault)
      localStorage.setItem('@qubit-environment',environment.name)
      environment = environment.name
    }
  }
  else{
    localStorage.setItem('@qubit-environment',envDefault)
    environment = envDefault
    notification('Cambiado a "'+envDefault+'"', 'warning', 3)
  }
  
  store.dispatch(
    dispacth => {
        dispacth({
            type:"ENVIRONMENTS",
            environment
        })
    } 
  )
}

function getEnvironment(envDefault=null){
  if(envDefault==null){
    return  env.environments.find(environment => environment.name === store.getState().environment)
  }
  return  env.environments.find(environment => environment.name === envDefault)
}

export function logout(){
  get('/ping')
  storeProfile({status:null, profiles:null, password:null})

}

export function request(url, options = {}){

  let environment = (options.environment===undefined)? getEnvironment() : getEnvironment(options.environment)

  options.url =  environment.apiBaseUrl+url

  options.withCredentials = true

  return axios(options)
  .then((response) => {
    //notification(url, 'warning', 6)
    //console.log(response.data)
    return {success:true, data:response.data}
  })
  .catch((error) => {
    // Error 😨
    if (error.response) {
      //headers(error.response.headers)
        /*
         * The request was made and the server responded with a
         * status code that falls out of the range of 2xx
         */
         if (error.response.data) {
          //console.log(error.response.data.code);
          //console.log(error.response.data.message);
          let code = error.response.data.code
          code = (code===0)? 500 : code
          error = {success:false, message:error.response.data.message, code:code}
         }
         else{
          //console.log(error.response.data);
          //console.log(error.response.status);
          //console.log(error.response.headers);
          error =  {success:false, message:'Error ', code:error.response.status}
         }
    } else if (error.request) {
        /*
         * The request was made but no response was received, `error.request`
         * is an instance of XMLHttpRequest in the browser and an instance
         * of http.ClientRequest in Node.js
         */
        //console.log(error.request);
        error = {success:false, message:'Error en Conexión', code:'404'}
    } else {
        // Something happened in setting up the request and triggered an Error
        //console.log('Error', error.message);
        error =  {success:false, message:error.message, code:'500'}
    }
    notification(error, 'danger', 6)
    return error
  })
}

export function notification(data, type='primary', time=3){
  store.dispatch(
    dispacth => {
        dispacth({
            type:"NOTIFICATIONS",
            notification:{data, type, time}
        })
    } 
  )
}

export function storeProfile(profile={}){
  let status = null
  let profiles = null
  let password = null
  if(profile.status!==null){
    status = {
      username:profile.status.username,
      firstName:profile.status.firstName,
      lastName:profile.status.lastName
    }
    profiles = profile.profiles
    password = profile.password
  }
  localStorage.setItem('@qubit-profile',JSON.stringify(status))

  store.dispatch(
    dispacth => {
        dispacth({
            type:"PROFILES",
            profile:(status==null)? null : {status, profiles}
        })
    } 
  )
  store.dispatch(
    dispacth => {
        dispacth({
            type:"PASSWORDS",
            password
        })
    } 
  )
}


