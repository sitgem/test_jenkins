
import React, {Component} from 'react';

import PerfectScrollbar from 'react-perfect-scrollbar';
import { Route, Switch, Redirect } from "react-router-dom";

import './Qubit.css';
import Navbar from "../template/navbars/Navbar";
import Footer from "../template/footer/Footer";
import Sidebar from "../template/sidebar/Sidebar";

import routes  from "../routes";

import Notifications from "../template/notifications/Notifications";

import { connect } from 'react-redux';
import { notifications } from '../redux/actions'
import store  from '../redux/store'

var ps;
class Qubit extends Component {
  _isMounted = false
  constructor(props) {
    super(props);
    this.name = 'qubit'
    this.routes = []
    this.state = {
      update:false,
      backgroundColor: "blue"
    };

    this.switchRoute = this.switchRoute.bind(this)
    this.setSidebarRef = this.setSidebarRef.bind(this);
  }

  mainPanel = React.createRef();

  handleColorClick = color => {

    this.setState({ backgroundColor: color });
  };
  
  setSidebarRef(element) {
    this.sidebarRef = element;
    if (navigator.platform.indexOf("Win") > -1 && element) {
      ps = new PerfectScrollbar(element, {
        suppressScrollX: true,
        suppressScrollY: false
      });
    }
  }

  componentWillMount() {
    //this.routes = this.setMenuRoutes(routes)
    this._isMounted = true;
  }
 
  componentDidMount() {
    store.subscribe( () => {
      if(store.getState().status==='PROFILES'){
        //if(this._isMounted) this.setState({update:!this.state.update})
        this.setState({update:!this.state.update})
      }
    })
  }
    
  componentWillUnmount() {
    this._isMounted = false;
    if (navigator.platform.indexOf("Win") > -1 && ps) {
      ps.destroy();
    }
    if (navigator.platform.indexOf("Win") > -1) {
      //ps.destroy();
      //document.body.classList.toggle("perfect-scrollbar-on");
    }
  }

  componentWillUpdate(e) {

  }

  componentDidUpdate(e) {
   
    if (e.history.action === "PUSH") {

      this.mainPanel.current.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }
  }
  /*
  setMenuRoutes(routes, menu){
    return menu.map((row) => {
      return {
        name:row.name,
        icon:row.icon,
        ...routes.find(route => route.slug === row.slug),
      }
    })
  }
  */
  switchRoute(routes){
  
    return (
      <Switch>
        {routes.map((prop, key) => {
          return (
            <Route
              path={'/'+ this.name + prop.path}
              component={prop.component}
              key={key}
              {...this.props} 
            />
          );
        })}
      </Switch>
    )
  }

  render() {
    //console.log(store.getState())
    if(!this._isMounted) return null

    if(this.props.reduxStore.profile===null){

      
      return (<Redirect to={{ pathname: '/login'}} /> )
    }
    else if(this.props.location.pathname==='/'+this.name){

      return (<Redirect to={{ pathname: this.name+'/mass-cancel' }} /> )
    }
    else if (undefined===routes.find(route => '/'+this.name+route.path === this.props.location.pathname)){
      let path = 'not-found'
      if(this.props.location.pathname.indexOf('/'+this.name)===-1){
        path = '/'+this.name+this.props.location.pathname
      }
      return (<Redirect to={{ pathname: path, errorPath:this.props.location.pathname }}  /> )
    }

    const {
      className,
    } = this.props;
       
    return (
      <div className={className+" wrapper"} ref={this.setSidebarRef}>
       {
       // JSON.stringify(this.routes)
       }
        <Notifications {...this.props} />
        <Sidebar
          routes={routes}
          layout={this.name}
          location={this.props.location}
          entity_id={1}
          backgroundColor={this.state.backgroundColor}
          environment={this.props.reduxStore.environment}
        />
        <div className="main-panel" ref={this.mainPanel}>
          <Navbar 
            {...this.props} 
            routes={routes}
            environment={this.props.reduxStore.environment}
            profile={this.props.reduxStore.profile.status}
          />
          { this.switchRoute(routes) }
          <Footer fluid />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {

  return {
    reduxStore:{
      profile:state.profile,
      notification:state.notification,
      environment:state.environment,
      password:state.password
    }
  }
}
const mapDispatchToProps = dispatch => {

  return {
    reduxActions:{
      notifications(value){ dispatch(notifications(value)) },
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Qubit);
