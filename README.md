<p>
<img src="https://assets.cdnar.net/assets/public/qubit/production/logo-qubit-azul.svg" width="150" height="150">
<img src="https://miro.medium.com/max/3200/1*yk5D5cQB3jd7EiPzrDrD5w.png" width="140" height="60">
</p>
<p align="center">
</p>


## Git
```bash
git clone http://git.qubit.tv:8888/vod/front-reactjs.git;
```
```bash
git checkout develop;
```

## Configuration

Configura las variables de entorno a usar como dirección de la API, entre otras.
```bash
gateway/
├── src/
│   └── environments.js
```
Puerto por defecto: PORT=8005
```bash
gateway/
├── package.json
```
## Installation

```bash
$ npm install; 
```

## Execute

```bash
$ npm start
```

## Structure

```bash
front-reactjs/
├── CHANGELOG.md
├── LICENSE.md
├── README.md
├── package.json
├── public
│   ├── apple-icon-qubit.png
│   ├── favicon-qubit.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── assets
    │   ├── css
    │   │   └── ...
    │   ├── fonts
    │   │   └── ...
    │   ├── img
    │   │   ├── flags
    │   │   │   └── ...
    │   │   └── ...
    │   └── sass
    │       ├── now-ui-dashboard
    │       │   ├── mixins
    │       │   │   └── ...
    │       │   ├── plugins
    │       │   │   └── ...
    │       │   └── ...
    │       └── now-ui-dashboard.scss
    ├── components
    │   ├── genres
    │   │   └── GenreList.jsx
    │   └── sas
    │       ├── CreditCardUpdate.jsx
    │       └── MassCancel.jsx
    ├── layout
    │   ├── Qubit.jsx
    │   └── Qubit.css
    ├── redux
    │   ├── actions.js
    │   └── store.js
    │
    ├── index.js
    ├── routes.js
    └── template
        ├── documentation
        │   └── tutorial-components.html
        ├── fixedPlugin
        │   └── FixedPlugin.jsx
        ├── footer
        │   └── Footer.jsx
        ├── login
        │   └── Login.jsx
        ├── navbars
        │   └── Navbars.jsx
        ├── notFound
        │   └── NotFound.jsx
        ├── notifications
        │   └── Notifications.jsx
        ├── panelHeader
        │   └── PanelHeader.jsx
        ├── sidebar
        │   └── Sidebar.jsx
        └── signup
            └── Signup.jsx

```

## Plantilla Usada 'Now UI Dashboard'

[<img src="https://s3.amazonaws.com/creativetim_bucket/github/react.svg" width="60" height="60" />](https://www.creative-tim.com/product/now-ui-dashboard-react)


| React |
| --- |
| [![Now UI Dashboard React](https://s3.amazonaws.com/creativetim_bucket/products/76/original/opt_nud_react_thumbnail.jpg)](https://www.creative-tim.com/product/now-ui-dashboard-react)
## Plantilla Demo

| Dashboard | User Profile | Tables | Maps | Notification |
| --- | --- | --- | --- | --- |
| [![Start page](./src/assets/github/dashboard.jpg)](https://demos.creative-tim.com/now-ui-dashboard-react/#/admin/dashboard) | [![User profile page](./src/assets/github/user_profile.jpg)](https://demos.creative-tim.com/now-ui-dashboard-react/#/admin/user) | [![Tables page ](./src/assets/github/tables.jpg)](https://demos.creative-tim.com/now-ui-dashboard-react/#/admin/table) | [![Maps Page](./src/assets/github/maps.jpg)](https://demos.creative-tim.com/now-ui-dashboard-react/#/admin/maps) | [![Notification page](./src/assets/github/notification.jpg)](https://demos.creative-tim.com/now-ui-dashboard-react/#/admin/notifications)

[ver pagina](https://demos.creative-tim.com/now-ui-dashboard-react/#/admin/dashboard).


## Plantilla Documentación
La documentación de Now UI Dashboard React está alojada en [creative-tim](https://demos.creative-tim.com/now-ui-dashboard-react/#/documentation/tutorial).
